import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { SubjectService } from '../subject.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit, OnDestroy {
  private subject: string;

  @Output() next = new EventEmitter()
  private subscription: Subscription;

  constructor(public subjectService: SubjectService) {
    this.subscription = this.subjectService.observable$.subscribe(value => {

      console.log(value);
      this.subject = value;
    });
  }

  ngOnInit() {
  }

  setValue() {

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
