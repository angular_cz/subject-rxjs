import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  private subject = new Subject<string>();
  readonly observable$ = this.subject.asObservable();

  constructor() {
  }

  setSubjectToRandom() {
    this.subject.next(Math.random().toString());
  }

  startTimer() {
    setTimeout(() => this.setSubjectToRandom(), 1000);
  }

}
